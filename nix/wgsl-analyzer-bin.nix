{ stdenv
, lib
, fetchurl
, autoPatchelfHook
}:

let
  pname = "wgsl-analyzer-bin";
  version = "0.6.2";
in
stdenv.mkDerivation {
  inherit pname version;

  src = fetchurl {
    url = "https://github.com/wgsl-analyzer/wgsl-analyzer/releases/download/v${version}/wgsl_analyzer-linux-x64";
    sha256 = "sha256-Y+fOs2Ingv/4SE67OKKGxrZ1Hu0/E91DObqV+xuEwaE=";
  };

  dontUnpack = true;

  buildInputs = [
    (stdenv.cc.cc.libgcc or null)
  ];

  nativeBuildInputs = [
    autoPatchelfHook
  ];

  installPhase = ''
    install -m755 -D $src $out/bin/wgsl_analyzer
  '';

  meta = with lib; {
    description = "A language server implementation for the WGSL shading language";
    homepage = "https://github.com/wgsl-analyzer/wgsl-analyzer";
  };
}
