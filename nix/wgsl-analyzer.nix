{ lib
, fetchFromGitHub
, rustPlatform
}:

rustPlatform.buildRustPackage rec {
  pname = "wgsl-analyzer";
  version = "0.6.2";

  src = fetchFromGitHub {
    owner = "wgsl-analyzer";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-T1lEMisEtb+6jVCu2TJNRLUskEREUPPEet033jbzpdY=";
  };

  cargoSha256 = "0000000000000000000000000000000000000000000000000000";

  auditable = true; # TODO: remove when this is the default

  meta = with lib; {
    description = "A language server implementation for the WGSL shading language";
    homepage = "https://github.com/wgsl-analyzer/wgsl-analyzer";
    license = with licenses; [ asl20 /* or */ mit ];
    maintainers = with maintainers; [ ];

    # Blocked by NixOS/nixpkgs#30742 since wgsl-analyzer uses multiple versions of naga
    broken = true;
  };
}
