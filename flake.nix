{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk/master";
    naersk.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        lib = pkgs.lib;
        naersk-lib = naersk.lib."${system}";

        runtime-libs = (with pkgs.xorg; [
          libX11
          libXcursor
          libXrandr
          libXi
        ]) ++ (with pkgs; [
          vulkan-loader
        ]);

        wgsl-analyzer = pkgs.callPackage ./nix/wgsl-analyzer.nix { };
        wgsl-analyzer-bin = pkgs.callPackage ./nix/wgsl-analyzer-bin.nix { };
      in
      {
        defaultPackage = naersk-lib.buildPackage {
          src = ./.;
          buildInputs = runtime-libs;
        };
        packages.wgsl-analyzer = wgsl-analyzer;
        packages.wgsl-analyzer-bin = wgsl-analyzer-bin;

        defaultApp = utils.lib.mkApp {
          drv = self.defaultPackage."${system}";
        };

        devShell = with pkgs; mkShell {
          nativeBuildInputs = [
            cargo
            rustc
            rustfmt
            pre-commit
            rustPackages.clippy
            rust-analyzer
            rnix-lsp
            taplo-cli

            wgsl-analyzer-bin
          ] ++ runtime-libs;
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
          LD_LIBRARY_PATH = lib.concatStringsSep ":" (map (lib: "${lib}/lib") runtime-libs);
        };
      });
}
