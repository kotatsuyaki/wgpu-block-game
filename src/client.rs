use std::collections::HashSet;
use std::time::{Duration, Instant};
use std::{
    collections::HashMap,
    f32::consts::{FRAC_PI_2, FRAC_PI_4, PI},
};

use anyhow::Result;
use bytemuck::{Pod, Zeroable};
use glam::{vec3, EulerRot, Mat4, Vec3};
use itertools::Itertools;
use tokio::runtime::Handle;
use tracing::{debug, error, info};
use wgpu::util::BufferInitDescriptor;
use wgpu::{
    include_wgsl, util::DeviceExt, Adapter, Backends, BindGroup, BindGroupDescriptor,
    BindGroupEntry, BindGroupLayout, BindGroupLayoutDescriptor, BindGroupLayoutEntry,
    BindingResource, BindingType, Buffer, BufferUsages, Device, DeviceDescriptor, Features,
    Instance, InstanceDescriptor, Limits, PowerPreference, PresentMode, SamplerBindingType,
    ShaderModule, ShaderStages, Surface, SurfaceConfiguration, TextureSampleType, TextureUsages,
    TextureViewDimension,
};
use wgpu::{CommandEncoder, TextureView};
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

use crate::client::texture::Atlas;
use crate::{
    message::{self, ServerMessage},
    utils::{AsyncChannel, AsyncSender, SyncChannel, SyncReceiver},
    world::World,
};

use self::remote::{InboundMessage, OutboundMessage, RemoteNetwork};
use self::render::RenderChunk;

mod remote;
mod render;
mod texture;
mod vertex;

#[allow(unused)]
pub struct Client {
    world: World,

    inbound_messages: SyncReceiver<InboundMessage>,
    outbound_messages: AsyncSender<OutboundMessage>,

    last_report: Instant,
    frame_count: u64,
}

pub fn new() -> Result<(Client, RemoteNetwork)> {
    let world = World::new();
    let inbound_messages = SyncChannel::new();
    let outbound_messages = AsyncChannel::new();

    let client = Client {
        world,
        inbound_messages: inbound_messages.receiver,
        outbound_messages: outbound_messages.sender,
        last_report: Instant::now(),
        frame_count: 0,
    };

    let remote_network = RemoteNetwork {
        inbound_messages: inbound_messages.sender,
        outbound_messages: outbound_messages.receiver,
    };

    Ok((client, remote_network))
}

impl Client {
    pub fn run(mut self, main_thread_runtime: Handle, multi_thread_runtime: Handle) -> Result<()> {
        let display = main_thread_runtime.block_on(Display::new(multi_thread_runtime))?;

        display.run(move |context, render_state| self.run_gameloop_body(context, render_state))
    }

    // The loop body that gets called in sync with render surface FPS
    pub fn run_gameloop_body(
        &mut self,
        context: GameLoopContext,
        render_state: &mut DisplayState,
    ) -> Result<()> {
        let elapsed = Instant::now() - self.last_report;
        self.frame_count += 1;
        if elapsed >= Duration::from_secs(1) {
            self.run_secondly_task(elapsed)?;
        }

        let (whole_ticks, frac_ticks) = {
            const TICK_MILLIS: u64 = 50;
            const TICK_MICROS: f64 = 50000.0;
            let whole_ticks = (elapsed.as_millis() as u64) / TICK_MILLIS;
            let frac_ticks = ((elapsed.as_micros() as f64) % TICK_MICROS) / TICK_MICROS;

            debug_assert!(
                frac_ticks >= 0.0 && frac_ticks < 1.0,
                "frac_ticks = {frac_ticks}"
            );

            (whole_ticks, frac_ticks)
        };

        self.handle_pending_inbound_messages()?;
        self.render_dirty_chunks(render_state)?;

        Ok(())
    }

    fn handle_pending_inbound_messages(&mut self) -> Result<()> {
        while let Ok(inbound_message) = self.inbound_messages.try_recv() {
            info!(
                "Client tick received inbound server message: {:?}",
                inbound_message
            );

            self.handle_inbound_message(inbound_message)?;
        }

        Ok(())
    }

    fn handle_inbound_message(&mut self, inbound_message: InboundMessage) -> Result<()> {
        match inbound_message.server_message {
            ServerMessage::Pong => {
                info!("Received server pong");
            }
            ServerMessage::UpdateChunk { coord, chunk } => {
                self.world.chunk_store.set_chunk(coord, chunk);
            }
            ServerMessage::UnloadChunk { coord } => {
                self.world.chunk_store.take_chunk(coord);
            }
        }

        Ok(())
    }

    fn render_dirty_chunks(&mut self, display_state: &mut DisplayState) -> Result<()> {
        let dirty_coords = self.find_chunk_coords_to_render();

        for coord in dirty_coords {
            let vertices = RenderChunk::new(coord, &self.world, &display_state.atlas).render();
            display_state.insert_rendered_chunk(coord, vertices);
            info!("Render chunk at {:?}", coord);
        }

        for (_, chunk) in self.world.chunk_store.iter_mut() {
            chunk.set_all_rendered();
        }

        Ok(())
    }

    fn find_chunk_coords_to_render(&self) -> Vec<(i64, i64, i64)> {
        let all_xy_coords = self.world.chunk_store.coords().cloned().sorted();
        let non_border_xy_coords = all_xy_coords.filter(|&(x, z)| {
            self.world.chunk_store.has_chunk_at((x + 1, z))
                && self.world.chunk_store.has_chunk_at((x - 1, z))
                && self.world.chunk_store.has_chunk_at((x, z + 1))
                && self.world.chunk_store.has_chunk_at((x, z - 1))
        });
        let all_coords = non_border_xy_coords
            .map(|(x, z)| (0_i64..16).map(move |y| (x, y, z)))
            .flatten();
        let all_dirty_coords = all_coords.filter(|&(x, y, z)| {
            self.world
                .chunk_store
                .chunk((x, z))
                .unwrap()
                .is_subchunk_rendered(y)
        });

        all_dirty_coords.collect()
    }

    fn run_secondly_task(&mut self, elapsed: Duration) -> Result<()> {
        self.outbound_messages.send(OutboundMessage {
            client_message: message::ClientMessage::Ping,
        })?;
        info!("Sent outbound ping message");

        let fps = (self.frame_count as f64) / elapsed.as_secs_f64();
        info!(fps);

        self.last_report = Instant::now();
        self.frame_count = 0;

        Ok(())
    }
}

pub struct Display {
    event_loop: EventLoop<()>,
    state: DisplayState,
    runtime: Handle,
}

pub struct GameLoopContext<'a> {
    runtime: &'a Handle,
}

impl Display {
    pub async fn new(runtime: Handle) -> Result<Self> {
        let event_loop = EventLoop::new();
        let window = WindowBuilder::new().build(&event_loop).unwrap();
        let state = DisplayState::new(window).await?;

        Ok(Self {
            event_loop,
            state,
            runtime,
        })
    }

    pub fn run<R, T>(self, mut run_gameloop_body: R) -> !
    where
        R: FnMut(GameLoopContext, &mut DisplayState) -> anyhow::Result<T> + 'static,
    {
        let Display {
            event_loop,
            mut state,
            runtime,
        } = self;

        event_loop.run(move |event, _, control_flow| {
            match event {
                Event::WindowEvent {
                    ref event,
                    window_id,
                } if window_id == state.window().id() => {
                    if !state.input(event) {
                        match event {
                            WindowEvent::CloseRequested
                            | WindowEvent::KeyboardInput {
                                input:
                                    KeyboardInput {
                                        state: ElementState::Pressed,
                                        virtual_keycode: Some(VirtualKeyCode::Escape),
                                        ..
                                    },
                                ..
                            } => *control_flow = ControlFlow::Exit,
                            WindowEvent::Resized(physical_size) => {
                                state.resize(*physical_size);
                            }
                            WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                                state.resize(**new_inner_size);
                            }
                            _ => {}
                        }
                    }
                }
                Event::RedrawRequested(window_id) if window_id == state.window().id() => {}
                Event::MainEventsCleared => {
                    let context = GameLoopContext { runtime: &runtime };
                    if let Err(e) = run_gameloop_body(context, &mut state) {
                        error!("Frame callback returned error: {:?}", e);
                        control_flow.set_exit_with_code(1);
                    }

                    match state.render() {
                        Ok(_) => {}
                        // Reconfigure the surface if lost
                        Err(wgpu::SurfaceError::Lost) => state.resize(state.size),
                        // The system is out of memory, we should probably quit
                        Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                        // All other errors (Outdated, Timeout) should be resolved by the next frame
                        Err(e) => eprintln!("{e:?}"),
                    }
                }
                _ => {}
            }
        })
    }
}

pub struct DisplayState {
    surface: Surface,
    device: Device,
    queue: wgpu::Queue,
    config: SurfaceConfiguration,
    size: winit::dpi::PhysicalSize<u32>,
    window: Window,
    render_pipeline: wgpu::RenderPipeline,

    atlas: Atlas,
    atlas_bind_group: BindGroup,

    camera: Camera,
    camera_buffer: Buffer,
    camera_bind_group: BindGroup,

    rendered_chunks: HashMap<(i64, i64, i64), Vec<vertex::Vertex>>,
    vertex_buffers: HashMap<(i64, i64, i64), (Buffer, usize)>,
}

pub struct RenderedChunk {
    vertices: Vec<vertex::Vertex>,
    buffer: Option<Buffer>,
}

pub struct Camera {
    eye: Vec3,
    pitch: f32,
    yaw: f32,
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Pod, Zeroable)]
struct CameraUniform {
    view_proj: Mat4,
}

impl DisplayState {
    async fn new(window: Window) -> Result<Self> {
        let size = window.inner_size();

        let instance = create_instance();
        let surface = unsafe { instance.create_surface(&window) }.unwrap();
        let adapter = create_adapter(instance, &surface).await;
        let (device, queue) = create_device_and_queue(&adapter).await;
        let config = configure_surface(&surface, adapter, size, &device);

        let atlas = Atlas::new(&device, &queue)?;
        let (atlas_bind_group, atlas_bind_layout) = create_atlas_bind_group(&device, &atlas);

        let camera = Camera::new(vec3(0.0, 100.0, 0.0), 0.0, PI - 0.05);
        let camera_buffer = create_camera_buffer(&device, &camera);
        let (camera_bind_group, camera_bind_group_layout) =
            create_camera_bind_group(&device, &camera_buffer);

        let shader = device.create_shader_module(include_wgsl!("./shaders/shader.wgsl"));
        let render_pipeline = create_render_pipeline(
            &device,
            atlas_bind_layout,
            camera_bind_group_layout,
            shader,
            &config,
        );
        let rendered_chunks = HashMap::new();
        let vertex_buffers = HashMap::new();

        Ok(Self {
            surface,
            device,
            queue,
            config,
            size,
            window,
            render_pipeline,
            atlas_bind_group,
            camera,
            camera_buffer,
            camera_bind_group,
            rendered_chunks,
            atlas,
            vertex_buffers,
        })
    }

    pub fn update_camera(&mut self, camera: Camera) {
        self.camera = camera;
        self.queue.write_buffer(
            &self.camera_buffer,
            0,
            bytemuck::cast_slice(&[CameraUniform {
                view_proj: self.camera.view_proj_matrix(),
            }]),
        );
    }

    fn window(&self) -> &Window {
        &self.window
    }

    fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        if new_size.width > 0 && new_size.height > 0 {
            self.size = new_size;
            self.config.width = new_size.width;
            self.config.height = new_size.height;
            self.surface.configure(&self.device, &self.config);
        }
    }

    fn input(&mut self, _event: &WindowEvent) -> bool {
        false
    }

    pub fn insert_rendered_chunk(&mut self, coord: (i64, i64, i64), vertices: Vec<vertex::Vertex>) {
        self.rendered_chunks.insert(coord, vertices);
    }

    pub fn remove_rendered_chunk(&mut self, coord: (i64, i64, i64)) {
        self.rendered_chunks.remove(&coord);
    }

    fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        let output = self.surface.get_current_texture()?;
        let output_texture_view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        self.update_vertex_buffers();
        self.render_blocks(&mut encoder, &output_texture_view);

        self.queue.submit(std::iter::once(encoder.finish()));
        output.present();

        Ok(())
    }

    fn render_blocks(&mut self, encoder: &mut CommandEncoder, output_texture_view: &TextureView) {
        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: Some("Render Pass"),
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: output_texture_view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color {
                        r: 0.1,
                        g: 0.2,
                        b: 0.3,
                        a: 1.0,
                    }),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });

        // Render vertex buffers in sorted order for easier debugging
        let all_vertex_buffer_coords = self.vertex_buffers.keys().cloned().sorted().collect_vec();

        for (_coord, (vertex_buffer, buffer_length)) in all_vertex_buffer_coords
            .iter()
            .map(|key| self.vertex_buffers.get_key_value(key).unwrap())
        {
            render_pass.set_pipeline(&self.render_pipeline);
            render_pass.set_bind_group(0, &self.atlas_bind_group, &[]);
            render_pass.set_bind_group(1, &self.camera_bind_group, &[]);
            render_pass.set_vertex_buffer(0, vertex_buffer.slice(..));
            render_pass.draw(0..(*buffer_length as u32), 0..1);
        }
    }

    /// Insert and remove vertex buffers to make them match the content of rendered chunks.
    fn update_vertex_buffers(&mut self) {
        let rendered_coords: HashSet<_> = self.rendered_chunks.keys().cloned().collect();
        let buffer_coords: HashSet<_> = self.vertex_buffers.keys().cloned().collect();

        let new_coords = rendered_coords
            .difference(&buffer_coords)
            .cloned()
            .collect_vec();
        let obsolete_coords = buffer_coords
            .difference(&rendered_coords)
            .cloned()
            .collect_vec();

        for &coord in new_coords.iter() {
            let rendered_chunk = &self.rendered_chunks[&coord];
            let vertex_buffer = self.device.create_buffer_init(&BufferInitDescriptor {
                label: Some("Vertex Buffer"),
                contents: bytemuck::cast_slice(rendered_chunk.as_slice()),
                usage: BufferUsages::VERTEX,
            });
            self.vertex_buffers
                .insert(coord, (vertex_buffer, rendered_chunk.len()));
        }

        for &coord in obsolete_coords.iter() {
            self.vertex_buffers.remove(&coord);
        }
    }
}

fn create_camera_bind_group(
    device: &Device,
    camera_buffer: &Buffer,
) -> (BindGroup, BindGroupLayout) {
    let bind_layout = create_camera_bind_layout(device);
    let bind_group = device.create_bind_group(&BindGroupDescriptor {
        label: Some("Camera Uniform Buffer Bind Group"),
        layout: &bind_layout,
        entries: &[BindGroupEntry {
            binding: 0,
            resource: camera_buffer.as_entire_binding(),
        }],
    });
    (bind_group, bind_layout)
}

fn create_atlas_bind_group(device: &Device, atlas: &Atlas) -> (BindGroup, BindGroupLayout) {
    let bind_layout = create_atlas_bind_layout(device);
    let bind_group = device.create_bind_group(&BindGroupDescriptor {
        layout: &bind_layout,
        entries: &[
            BindGroupEntry {
                binding: 0,
                resource: BindingResource::TextureView(&atlas.view()),
            },
            BindGroupEntry {
                binding: 1,
                resource: BindingResource::Sampler(&atlas.sampler()),
            },
        ],
        label: Some("Atlas Bind Group"),
    });
    (bind_group, bind_layout)
}

fn create_camera_bind_layout(device: &Device) -> BindGroupLayout {
    device.create_bind_group_layout(&BindGroupLayoutDescriptor {
        label: Some("Camera Uniform Buffer Bind Group Layout"),
        entries: &[BindGroupLayoutEntry {
            binding: 0,
            visibility: ShaderStages::VERTEX,
            ty: BindingType::Buffer {
                ty: wgpu::BufferBindingType::Uniform,
                has_dynamic_offset: false,
                min_binding_size: None,
            },
            count: None,
        }],
    })
}

fn create_instance() -> Instance {
    let instance = Instance::new(InstanceDescriptor {
        backends: Backends::all(),
        ..Default::default()
    });
    instance
}

fn configure_surface(
    surface: &Surface,
    adapter: Adapter,
    size: winit::dpi::PhysicalSize<u32>,
    device: &Device,
) -> SurfaceConfiguration {
    let surface_caps = surface.get_capabilities(&adapter);
    let surface_format = *surface
        .get_capabilities(&adapter)
        .formats
        .iter()
        .find(|f| f.describe().srgb)
        .unwrap();
    let config = SurfaceConfiguration {
        usage: TextureUsages::RENDER_ATTACHMENT,
        format: surface_format,
        width: size.width,
        height: size.height,
        present_mode: PresentMode::AutoVsync,
        alpha_mode: surface_caps.alpha_modes[0],
        view_formats: vec![],
    };
    surface.configure(device, &config);
    config
}

async fn create_adapter(instance: Instance, surface: &Surface) -> Adapter {
    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            compatible_surface: Some(surface),
            power_preference: PowerPreference::default(),
            force_fallback_adapter: false,
        })
        .await
        .unwrap();
    adapter
}

async fn create_device_and_queue(adapter: &Adapter) -> (Device, wgpu::Queue) {
    let (device, queue) = adapter
        .request_device(
            &DeviceDescriptor {
                features: Features::empty(),
                limits: Limits::default(),
                label: None,
            },
            None,
        )
        .await
        .unwrap();
    (device, queue)
}

fn create_atlas_bind_layout(device: &Device) -> BindGroupLayout {
    device.create_bind_group_layout(&BindGroupLayoutDescriptor {
        label: Some("Texture Bind Group Layout"),
        entries: &[
            BindGroupLayoutEntry {
                binding: 0,
                visibility: ShaderStages::FRAGMENT,
                ty: BindingType::Texture {
                    sample_type: TextureSampleType::Float { filterable: true },
                    view_dimension: TextureViewDimension::D2,
                    multisampled: false,
                },
                count: None,
            },
            BindGroupLayoutEntry {
                binding: 1,
                visibility: ShaderStages::FRAGMENT,
                ty: BindingType::Sampler(SamplerBindingType::Filtering),
                count: None,
            },
        ],
    })
}

fn create_render_pipeline(
    device: &Device,
    atlas_bind_group_layout: BindGroupLayout,
    camera_bind_group_layout: BindGroupLayout,
    shader: ShaderModule,
    config: &SurfaceConfiguration,
) -> wgpu::RenderPipeline {
    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: Some("Render Pipeline"),
        layout: Some(
            &device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Render Pipeline Layout"),
                bind_group_layouts: &[&atlas_bind_group_layout, &camera_bind_group_layout],
                push_constant_ranges: &[],
            }),
        ),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main",
            buffers: &[vertex::Vertex::buffer_layout()],
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            targets: &[Some(wgpu::ColorTargetState {
                format: config.format,
                blend: Some(wgpu::BlendState::REPLACE),
                write_mask: wgpu::ColorWrites::ALL,
            })],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: Some(wgpu::Face::Back),
            unclipped_depth: false,
            polygon_mode: wgpu::PolygonMode::Fill,
            conservative: false,
        },
        depth_stencil: None,
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
    })
}

fn create_camera_buffer(device: &Device, camera: &Camera) -> Buffer {
    device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Camera Uniform Buffer"),
        contents: bytemuck::cast_slice(&[CameraUniform {
            view_proj: camera.view_proj_matrix(),
        }]),
        usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
    })
}

impl Camera {
    const UP: Vec3 = vec3(0.0, 1.0, 0.0);

    /// In practice, pitch (0 - pi) is applied first, then yaw (0 - 2pi).
    /// (yaw = 0, pitch = ?) corresponds to looking upwards.
    fn new(eye: Vec3, yaw: f32, pitch: f32) -> Self {
        Self { eye, yaw, pitch }
    }

    fn view_proj_matrix(&self) -> Mat4 {
        let up = if approx::ulps_eq!(self.pitch, PI) {
            // Fix the gimbal lock issue when looking downwards by using another up vector
            transform_point(Self::UP, FRAC_PI_2, self.yaw)
        } else {
            Self::UP
        };

        let front = transform_point(Self::UP, self.pitch, self.yaw);
        let view = Mat4::look_to_lh(self.eye, front, up);
        let proj = Mat4::perspective_lh(FRAC_PI_4, 800.0 / 600.0, 0.1, 1000.0);

        debug!("front = {front}");
        debug!("view = {view}");
        debug!("proj = {proj}");

        proj * view
    }
}

fn transform_point(point: Vec3, pitch: f32, yaw: f32) -> Vec3 {
    Mat4::from_euler(EulerRot::YXZ, yaw, pitch, 0.0).transform_point3(point)
}

#[cfg(test)]
mod test {
    use std::f32::consts::SQRT_2;

    use approx::assert_ulps_eq;
    use glam::EulerRot;

    use super::*;

    #[test]
    fn test_transform_point() {
        // Apply 45 deg pitch (from y-axis to z-axis) and 45 deg yaw (from z-axis to x-axis).
        let input = Camera::UP;
        let pitch = FRAC_PI_4;
        let yaw = FRAC_PI_4;
        let output = transform_point(input, pitch, yaw);

        let expected = vec3(
            SQRT_2.recip().powi(2),
            SQRT_2.recip(),
            SQRT_2.recip().powi(2),
        );

        assert_ulps_eq!(output, expected);
        assert_ulps_eq!(output.length(), 1.0);
    }

    #[test]
    fn test_glam_eulerrot() {
        // EulerRot::YXZ results in the following behavior:
        // - The three angle (f32) arguments in Mat4::from_euler is in the order of Y, X, Z
        // - Rotation along the Y-axis is from Z-axis to X-axis (yaw)
        // - Rotation along the X-axis is from Y-axis to Z-axis (pitch)
        // - Rotation along the Z-axis is from X-axis to Y-axis (also pitch?)
        //
        // The above facts can be deduced by reading "YXZ" backwards in a wrapping manner.
        // Angles are in radians.
        // The three angle arguments correspond to yaw, pitch, and roll.

        let mat = Mat4::from_euler(EulerRot::YXZ, std::f32::consts::FRAC_PI_4, 0.0, 0.0);
        let point = vec3(1.0, 1.0, 1.0);
        let expected = vec3(std::f32::consts::SQRT_2, 1.0, 0.0);
        let output = mat.transform_point3(point);
        assert!(
            expected.abs_diff_eq(output, f32::EPSILON),
            "Expected {mat} * {point} to be {expected}; Found {output}"
        );

        let mat = Mat4::from_euler(EulerRot::YXZ, 0.0, std::f32::consts::FRAC_PI_4, 0.0);
        let point = vec3(1.0, 1.0, 1.0);
        let expected = vec3(1.0, 0.0, std::f32::consts::SQRT_2);
        let output = mat.transform_point3(point);
        assert!(
            expected.abs_diff_eq(output, f32::EPSILON),
            "Expected {mat} * {point} to be {expected}; Found {output}"
        );

        let mat = Mat4::from_euler(EulerRot::YXZ, 0.0, 0.0, std::f32::consts::FRAC_PI_4);
        let point = vec3(1.0, 1.0, 1.0);
        let expected = vec3(0.0, std::f32::consts::SQRT_2, 1.0);
        let output = mat.transform_point3(point);
        assert!(
            expected.abs_diff_eq(output, f32::EPSILON),
            "Expected {mat} * {point} to be {expected}; Found {output}"
        );
    }
}
