use std::net::SocketAddr;

use anyhow::Result;
use tokio::runtime::Runtime;
use tracing::level_filters::LevelFilter;
use tracing_subscriber::{EnvFilter, FmtSubscriber};
use utils::{report_errors, SyncChannel, SyncReceiver};

mod client;
mod server;

mod message;

mod block;
mod chunk;
mod entity;
mod utils;
mod world;

fn main() -> Result<()> {
    init_logging()?;

    let server_runtime = ServerRuntime::new()?;
    let local_address_receiver = server_runtime.start_local_server()?;

    let client_runtime = ClientRuntime::new()?;
    client_runtime.run(local_address_receiver)?;

    Ok(())
}

struct ClientRuntime {
    main_thread_runtime: Runtime,
    multi_thread_runtime: Runtime,
}

impl ClientRuntime {
    fn new() -> Result<Self> {
        let main_thread_runtime = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()?;
        let multi_thread_runtime = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()?;
        Ok(Self {
            main_thread_runtime,
            multi_thread_runtime,
        })
    }

    fn run(&self, local_address_receiver: SyncReceiver<SocketAddr>) -> Result<()> {
        let (client, remote_network) = client::new()?;
        {
            let server_address = local_address_receiver.recv()?;

            let _guard = self.multi_thread_runtime.enter();
            remote_network.connect_to_server(server_address);
        }
        client.run(
            self.main_thread_runtime.handle().clone(),
            self.multi_thread_runtime.handle().clone(),
        )?;
        Ok(())
    }
}

struct ServerRuntime {
    runtime: Runtime,
}

impl ServerRuntime {
    fn new() -> Result<Self> {
        let runtime = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()?;
        Ok(Self { runtime })
    }

    fn start_local_server(&self) -> Result<SyncReceiver<SocketAddr>> {
        let SyncChannel {
            sender: local_address_sender,
            receiver: local_address_receiver,
        } = SyncChannel::new_bounded(1);

        self.runtime.spawn(report_errors(async move {
            let (mut server, remote_network) = server::new()?;
            let local_address = remote_network.local_connect_address();
            let listen_task = remote_network.listen_on_endpoint();

            local_address_sender.send(local_address)?;

            tokio::task::spawn_blocking(move || server.run_tick_loop()).await??;
            listen_task.await?;

            Ok(())
        }));

        Ok(local_address_receiver)
    }
}

fn init_logging() -> Result<()> {
    let filter = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .with_env_var("RUST_LOG")
        .from_env()?;
    let subscriber = FmtSubscriber::builder().with_env_filter(filter).finish();
    tracing::subscriber::set_global_default(subscriber)?;
    Ok(())
}
