use std::collections::{HashMap, HashSet};
use std::time::Instant;

use anyhow::Result;
use itertools::{iproduct, Itertools};
use quinn::{Endpoint, ServerConfig};
use spin_sleep::LoopHelper;
use tokio::spawn;
use tracing::{error, info};
use uuid::Uuid;

use crate::entity::EntityBehavior;
use crate::message;
use crate::utils::{AsyncSender, SyncChannel, SyncReceiver};
use crate::world::World;

use self::remote::{InboundMessage, OutboundMessage, Remote, RemoteNetwork};

pub struct Server {
    world: World,

    inbound_messages: SyncReceiver<InboundMessage>,
    new_remotes: SyncReceiver<Remote>,
    active_remotes: HashMap<Uuid, Remote>,

    loading_chunks: HashSet<(i64, i64)>,

    chunk_requests: AsyncSender<(i64, i64)>,
    loaded_chunks: SyncReceiver<chunk_provider::LoadChunkOutput>,
}

pub fn new() -> Result<(Server, RemoteNetwork)> {
    let world = World::new();

    let (cert, key) = remote::generate_self_signed_cert()?;
    let server_config = ServerConfig::with_single_cert(vec![cert], key)?;

    let endpoint = Endpoint::server(server_config, "[::]:0".parse()?)?;
    let inbound_messages = SyncChannel::new();
    let new_clients = SyncChannel::new();
    let remotes = HashMap::new();

    let (chunk_provider, chunk_requests, loaded_chunks) = chunk_provider::ChunkProvider::new();
    spawn(chunk_provider.start());

    let server = Server {
        world,
        inbound_messages: inbound_messages.receiver,
        new_remotes: new_clients.receiver,
        active_remotes: remotes,
        loading_chunks: HashSet::new(),
        chunk_requests,
        loaded_chunks,
    };

    let remote_network = RemoteNetwork {
        endpoint,
        inbound_messages: inbound_messages.sender,
        new_clients: new_clients.sender,
    };

    Ok((server, remote_network))
}

impl Server {
    /// Start running the server tick loop.
    ///
    /// This function is intended to be called from within a tokio runtime context, because it
    /// spawns tasks.
    pub fn run_tick_loop(&mut self) -> Result<()> {
        let mut loop_helper = LoopHelper::builder()
            .report_interval_s(5.0)
            .build_with_target_rate(20);
        loop {
            let _elapsed_time = loop_helper.loop_start();
            if let Some(fps) = loop_helper.report_rate() {
                info!(fps);
            }

            self.run_gameloop_body()?;

            loop_helper.loop_sleep();
        }
    }

    fn run_gameloop_body(&mut self) -> Result<()> {
        while let Ok(remote) = self.new_remotes.try_recv() {
            self.active_remotes.insert(remote.client_id(), remote);
        }

        self.process_inbound_messages()?;

        let load_coords = self.find_load_coords();
        self.dispatch_chunk_load(load_coords)?;

        let unload_coords = vec![];
        self.dispatch_chunk_unload(unload_coords);

        self.retrieve_chunk_load();
        self.send_chunks_to_remotes();

        Ok(())
    }

    fn dispatch_chunk_load(&mut self, load_coords: Vec<(i64, i64)>) -> Result<()> {
        for coord in load_coords {
            self.chunk_requests.send(coord)?;
            self.loading_chunks.insert(coord);
        }
        Ok(())
    }

    fn retrieve_chunk_load(&mut self) {
        while let Ok(output) = self.loaded_chunks.try_recv() {
            info!(
                "Receiving generated chunk at {:?} from provider task",
                output.coord
            );

            self.loading_chunks.remove(&output.coord);
            match output.result {
                Ok(chunk) => {
                    info!("Loaded chunk at {:?}", output.coord);
                    self.world.chunk_store.set_chunk(output.coord, chunk);
                }
                Err(e) => {
                    error!("Failed to load chunk at {:?}: {:?}", output.coord, e);
                }
            }
        }
    }

    fn dispatch_chunk_unload(&mut self, unload_coords: Vec<(i64, i64)>) {
        for coord in unload_coords {
            self.world.chunk_store.take_chunk(coord);
        }
    }

    /// Find chunk coordinates that should be loaded.
    fn find_load_coords(&self) -> Vec<(i64, i64)> {
        let expected_coords = iproduct!(-3_i64..=3, -3_i64..=3).collect::<HashSet<_>>();
        let loaded_coords = self
            .world
            .chunk_store
            .loaded_coordinates()
            .cloned()
            .collect::<HashSet<_>>();
        let loaded_and_loaded_coords = loaded_coords
            .union(&self.loading_chunks)
            .cloned()
            .collect::<HashSet<_>>();
        expected_coords
            .difference(&loaded_and_loaded_coords)
            .cloned()
            .collect_vec()
    }

    fn process_inbound_messages(&self) -> Result<()> {
        let drain_start_time = Instant::now();

        while let Ok(message) = self.inbound_messages.try_recv() {
            info!("Server tick loop received inbound message: {:?}", message);

            if matches!(message.client_message, message::ClientMessage::Ping) {
                if let Some(remote) = self.active_remotes.get(&message.client_id) {
                    remote.send(OutboundMessage {
                        server_message: message::ServerMessage::Pong,
                    })?;
                }
            }

            if message.received_time > drain_start_time {
                break;
            }
        }

        Ok(())
    }

    fn send_chunks_to_remotes(&mut self) {
        let loaded_coords = self
            .world
            .chunk_store
            .loaded_coordinates()
            .cloned()
            .collect::<HashSet<_>>();

        let remotes = &mut self.active_remotes;
        for (_uuid, remote) in remotes.iter_mut() {
            let coords_to_load = loaded_coords
                .difference(remote.loaded_coords())
                .cloned()
                .collect_vec();
            for coord in coords_to_load {
                let Some(chunk) = self.world.chunk_store.chunk(coord) else {
                        continue;
                    };
                remote.add_loaded_coord(coord);

                let _ = remote.send(OutboundMessage {
                    server_message: message::ServerMessage::UpdateChunk {
                        coord,
                        chunk: chunk.clone(),
                    },
                });
            }
        }
    }
}

/// Remote network communication & book keeping
mod remote {
    use std::collections::HashSet;
    use std::net::SocketAddr;
    use std::time::Instant;

    use anyhow::Result;
    use futures::{sink, SinkExt, StreamExt, TryStreamExt};
    use quinn::{self, Endpoint};
    use tokio::task::JoinHandle;
    use tokio_stream::wrappers::UnboundedReceiverStream;
    use tokio_util::codec::{FramedRead, FramedWrite, LengthDelimitedCodec};
    use tracing::info;
    use uuid::Uuid;

    use crate::message::{self, ClientMessage, ServerMessage};
    use crate::utils::{self, AsyncChannel, AsyncSender, SelfSerialize, SyncSender};

    pub struct RemoteNetwork {
        pub(super) endpoint: Endpoint,
        pub(super) inbound_messages: SyncSender<InboundMessage>,
        pub(super) new_clients: SyncSender<Remote>,
    }

    pub struct Remote {
        client_id: Uuid,
        outbound_sender: AsyncSender<OutboundMessage>,
        loaded_coords: HashSet<(i64, i64)>,
    }

    #[derive(Debug)]
    pub struct InboundMessage {
        pub client_id: Uuid,
        pub client_message: ClientMessage,
        pub received_time: Instant,
    }

    #[derive(Debug)]
    pub struct OutboundMessage {
        pub(super) server_message: ServerMessage,
    }

    impl RemoteNetwork {
        /// Spawns a listening task on the endpoint and return the [`JoinHandle`] to it.
        pub fn listen_on_endpoint(&self) -> JoinHandle<()> {
            let endpoint = self.endpoint.clone();

            tokio::spawn(utils::report_errors(listen_on_endpoint(
                endpoint,
                self.inbound_messages.clone(),
                self.new_clients.clone(),
            )))
        }

        /// Get the [`SocketAddr`] that can be used to connect to the server from the host itself.
        pub fn local_connect_address(&self) -> SocketAddr {
            let mut addr = self
                .endpoint
                .local_addr()
                .expect("Failed to get local address of server socket");

            addr.set_ip("::1".parse().unwrap());
            addr
        }
    }

    impl Remote {
        pub fn client_id(&self) -> Uuid {
            self.client_id
        }

        pub fn send(&self, message: OutboundMessage) -> Result<()> {
            self.outbound_sender.send(message)?;
            Ok(())
        }

        pub fn loaded_coords(&self) -> &HashSet<(i64, i64)> {
            &self.loaded_coords
        }

        pub fn add_loaded_coord(&mut self, coord: (i64, i64)) {
            self.loaded_coords.insert(coord);
        }
    }

    pub(crate) async fn listen_on_endpoint(
        endpoint: Endpoint,
        inbound_messages: SyncSender<InboundMessage>,
        new_remotes: SyncSender<Remote>,
    ) -> Result<()> {
        while let Some(conn) = endpoint.accept().await {
            let connection = conn.await?;

            let client_id = Uuid::new_v4();
            let AsyncChannel {
                sender: outbound_sender,
                receiver: outbound_receiver,
            } = AsyncChannel::new();

            info!("Assigned uuid {client_id} to new connection {connection:?}");

            new_remotes.send(Remote {
                client_id,
                outbound_sender,
                loaded_coords: HashSet::new(),
            })?;

            let inbound_sender = inbound_messages.clone();
            tokio::spawn(utils::report_errors(async move {
                let (sender, receiver) = connection.accept_bi().await?;

                tokio::spawn(utils::report_errors(forward_outbound_messages(
                    sender,
                    outbound_receiver,
                )));

                tokio::spawn(utils::report_errors(forward_inbound_messages(
                    receiver,
                    inbound_sender,
                    client_id,
                )));

                info!("Started forwarding messages for client {client_id}");

                Ok(())
            }));
        }

        Ok(())
    }

    pub(crate) async fn forward_outbound_messages(
        sender: quinn::SendStream,
        outbound_receiver: tokio::sync::mpsc::UnboundedReceiver<OutboundMessage>,
    ) -> Result<()> {
        let outbound_message_sink = FramedWrite::new(sender, LengthDelimitedCodec::new())
            .sink_map_err(anyhow::Error::from)
            .with(|outbound_message: OutboundMessage| async move {
                outbound_message.server_message.serialize().map(Into::into)
            });

        UnboundedReceiverStream::new(outbound_receiver)
            .map(Result::Ok)
            .forward(outbound_message_sink)
            .await
    }

    pub(crate) async fn forward_inbound_messages(
        receiver: quinn::RecvStream,
        inbound_sender: SyncSender<InboundMessage>,
        client_id: Uuid,
    ) -> Result<()> {
        FramedRead::new(receiver, LengthDelimitedCodec::new())
            .map_err(anyhow::Error::from)
            .and_then(|buf| async move {
                let client_message = message::ClientMessage::deserialize(buf)?;
                Ok(InboundMessage {
                    client_id,
                    client_message,
                    received_time: Instant::now(),
                })
            })
            // forward InboundMessage's into a sink that sends messages to the server tick loop
            .forward(sink::unfold(
                inbound_sender,
                |inbound_sender, inbound_message| async {
                    inbound_sender.send(inbound_message)?;
                    Ok(inbound_sender)
                },
            ))
            .await
    }

    pub(crate) fn generate_self_signed_cert() -> Result<(rustls::Certificate, rustls::PrivateKey)> {
        let cert = rcgen::generate_simple_self_signed(vec!["localhost".to_string()])?;
        let key = rustls::PrivateKey(cert.serialize_private_key_der());
        Ok((rustls::Certificate(cert.serialize_der()?), key))
    }
}

mod chunk_provider {
    use anyhow::Result;
    use tracing::info;

    use crate::chunk::Chunk;
    use crate::utils::AsyncChannel;
    use crate::utils::AsyncReceiver;
    use crate::utils::AsyncSender;
    use crate::utils::SyncChannel;
    use crate::utils::SyncReceiver;
    use crate::utils::SyncSender;

    pub(crate) struct ChunkProvider {
        pub(crate) chunk_requests: AsyncReceiver<(i64, i64)>,
        pub(crate) loaded_chunks: SyncSender<LoadChunkOutput>,
    }

    impl ChunkProvider {
        pub(crate) fn new() -> (Self, AsyncSender<(i64, i64)>, SyncReceiver<LoadChunkOutput>) {
            let chunk_requests = AsyncChannel::new();
            let loaded_chunks = SyncChannel::new();

            (
                Self {
                    chunk_requests: chunk_requests.receiver,
                    loaded_chunks: loaded_chunks.sender,
                },
                chunk_requests.sender,
                loaded_chunks.receiver,
            )
        }

        pub(crate) async fn start(mut self) -> Result<()> {
            while let Some((cx, cz)) = self.chunk_requests.recv().await {
                info!("Sending generated chunk at {:?} to server task", (cx, cz));
                self.loaded_chunks.send(LoadChunkOutput {
                    coord: (cx, cz),
                    result: Ok(Chunk::new()),
                })?;
            }

            Ok(())
        }
    }

    pub(crate) struct LoadChunkOutput {
        pub(crate) coord: (i64, i64),
        pub(crate) result: Result<Chunk>,
    }
}
