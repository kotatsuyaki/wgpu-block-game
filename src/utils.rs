use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::future::Future;

pub type SyncSender<T> = crossbeam_channel::Sender<T>;
pub type SyncReceiver<T> = crossbeam_channel::Receiver<T>;
pub type AsyncSender<T> = tokio::sync::mpsc::UnboundedSender<T>;
pub type AsyncReceiver<T> = tokio::sync::mpsc::UnboundedReceiver<T>;

pub struct SyncChannel<T> {
    pub sender: SyncSender<T>,
    pub receiver: SyncReceiver<T>,
}

pub struct AsyncChannel<T> {
    pub sender: AsyncSender<T>,
    pub receiver: AsyncReceiver<T>,
}

impl<T> SyncChannel<T> {
    pub fn new() -> Self {
        let (sender, receiver) = crossbeam_channel::unbounded();
        Self { sender, receiver }
    }

    pub fn new_bounded(cap: usize) -> Self {
        let (sender, receiver) = crossbeam_channel::bounded(cap);
        Self { sender, receiver }
    }
}

impl<T> AsyncChannel<T> {
    pub fn new() -> Self {
        let (sender, receiver) = tokio::sync::mpsc::unbounded_channel();
        Self { sender, receiver }
    }
}

/// Runs the supplied `inner_future` future and reports error if the output is an `Err`.
///
/// The output type of `inner_future` must be a `Result`.
pub async fn report_errors<T>(inner_future: impl Future<Output = Result<T>>) {
    if let Err(e) = inner_future.await {
        tracing::error!("{:#?}", e);
        tracing::error!("{}", e.backtrace());
    }
}

pub trait SelfSerialize
where
    Self: Serialize + for<'a> Deserialize<'a>,
{
    fn serialize(&self) -> Result<Vec<u8>> {
        Ok(bincode::serialize(self)?)
    }

    fn deserialize<T: AsRef<[u8]>>(bytes: T) -> Result<Self> {
        Ok(bincode::deserialize(bytes.as_ref())?)
    }
}

impl<T> SelfSerialize for T where T: Serialize + for<'a> Deserialize<'a> {}
