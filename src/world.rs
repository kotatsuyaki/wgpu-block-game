use itertools::Itertools;

use crate::{
    block::Block,
    chunk::ChunkStore,
    entity::{EntityBehavior, EntityStore},
};

pub struct World {
    pub chunk_store: ChunkStore,
    pub entity_store: EntityStore,
}

impl World {
    pub fn new() -> Self {
        World {
            chunk_store: ChunkStore::new(),
            entity_store: EntityStore::new(),
        }
    }

    /// Get an instance of the block at `(x, y, z)`.
    pub fn block(&self, (x, y, z): (i64, i64, i64)) -> Option<Block> {
        let (cx, cz) = (x.div_euclid(16), z.div_euclid(16));
        let (x, z) = (x.rem_euclid(16), z.rem_euclid(16));
        let chunk = self.chunk_store.chunk((cx, cz))?;
        Some(chunk.block((x, y, z)))
    }

    /// Get an instance of the block at `(x, y, z)` where `y` can be out of bounds.
    pub fn block_or_empty(&self, (x, y, z): (i64, i64, i64)) -> Option<Block> {
        let (cx, cz) = (x.div_euclid(16), z.div_euclid(16));
        let (x, z) = (x.rem_euclid(16), z.rem_euclid(16));
        let chunk = self.chunk_store.chunk((cx, cz))?;
        Some(chunk.block_or_empty((x, y, z)))
    }

    pub fn update_entities(&mut self) {
        let all_query_data = self
            .entity_store
            .iter()
            .filter_map(|(&id, entity)| entity.query(&self).map(|query_data| (id, query_data)))
            .collect_vec();
        for (id, query_data) in all_query_data.iter() {}
    }
}
