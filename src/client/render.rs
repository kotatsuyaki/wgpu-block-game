use glam::{vec3, Vec3};
use itertools::iproduct;

use crate::{
    block::{Block, BlockBehavior, BlockRenderKind, Facing},
    world::World,
};

use super::{
    texture::Atlas,
    vertex::{vertex, Vertex},
};

pub struct RenderChunk<'wo, 'at> {
    coord: (i64, i64, i64),
    world: &'wo World,
    atlas: &'at Atlas,
    vertices: Vec<Vertex>,
}

impl<'wo, 'at> RenderChunk<'wo, 'at> {
    pub fn new(coord: (i64, i64, i64), world: &'wo World, atlas: &'at Atlas) -> Self {
        Self {
            coord,
            world,
            atlas,
            vertices: vec![],
        }
    }

    pub fn render(mut self) -> Vec<Vertex> {
        let (cx, cy, cz) = self.coord;
        for (y, z, x) in iproduct!(0..16, 0..16, 0..16) {
            let world_block_coord = (x + cx * 16, y + cy * 16, z + cz * 16);
            let block = self.world.block(world_block_coord).unwrap();
            match block.render_kind() {
                BlockRenderKind::None => {}
                BlockRenderKind::Solid => self.render_solid_block(&block, world_block_coord),
            }
        }

        self.vertices
    }

    fn render_solid_block(&mut self, block: &Block, world_block_coord: (i64, i64, i64)) {
        let (x, y, z) = world_block_coord;
        let world_offset = vec3_i64(x, y, z);
        for facing in Facing::iter_variants() {
            let adjacent_world_block_coord = facing.displace((x, y, z));
            let adjacent_block = self
                .world
                .block_or_empty(adjacent_world_block_coord)
                .unwrap();
            if block.should_render_facing(facing, &adjacent_block) == false {
                continue;
            }

            let [pa, pb, pc, pd] = facing.into_quad();
            let texture_index = block.texture_facing(facing);
            let [ta, tb, tc, td] = self.atlas.box_for(block, texture_index);
            self.vertices.extend_from_slice(&[
                vertex(pa + world_offset, [0.19, 0.65, 0.32], ta),
                vertex(pb + world_offset, [0.19, 0.65, 0.32], tb),
                vertex(pc + world_offset, [0.19, 0.65, 0.32], tc),
                vertex(pc + world_offset, [0.19, 0.65, 0.32], tc),
                vertex(pd + world_offset, [0.19, 0.65, 0.32], td),
                vertex(pa + world_offset, [0.19, 0.65, 0.32], ta),
            ]);
        }
    }
}

fn vec3_i64(x: i64, y: i64, z: i64) -> Vec3 {
    vec3(x as f32, y as f32, z as f32)
}

trait IntoQuad {
    fn into_quad(self) -> [Vec3; 4];
}

impl IntoQuad for Facing {
    fn into_quad(self) -> [Vec3; 4] {
        match self {
            Facing::PosX => [
                vec3(1.0, 0.0, 0.0),
                vec3(1.0, 0.0, 1.0),
                vec3(1.0, 1.0, 1.0),
                vec3(1.0, 1.0, 0.0),
            ],
            Facing::NegX => [
                vec3(0.0, 0.0, 1.0),
                vec3(0.0, 0.0, 0.0),
                vec3(0.0, 1.0, 0.0),
                vec3(0.0, 1.0, 1.0),
            ],
            Facing::PosY => [
                vec3(0.0, 1.0, 0.0),
                vec3(1.0, 1.0, 0.0),
                vec3(1.0, 1.0, 1.0),
                vec3(0.0, 1.0, 1.0),
            ],
            Facing::NegY => [
                vec3(0.0, 0.0, 1.0),
                vec3(1.0, 0.0, 1.0),
                vec3(1.0, 0.0, 0.0),
                vec3(0.0, 0.0, 0.0),
            ],
            Facing::PosZ => [
                vec3(1.0, 0.0, 1.0),
                vec3(0.0, 0.0, 1.0),
                vec3(0.0, 1.0, 1.0),
                vec3(1.0, 1.0, 1.0),
            ],
            Facing::NegZ => [
                vec3(0.0, 0.0, 0.0),
                vec3(1.0, 0.0, 0.0),
                vec3(1.0, 1.0, 0.0),
                vec3(0.0, 1.0, 0.0),
            ],
        }
    }
}
