use bytemuck::{Pod, Zeroable};
use glam::{Vec2, Vec3};
use wgpu::{vertex_attr_array, BufferAddress, VertexAttribute, VertexBufferLayout};

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
pub struct Vertex {
    pub(crate) position: [f32; 3],
    pub(crate) tint: [f32; 3],
    pub(crate) tex: [f32; 2],
}

impl Vertex {
    pub(crate) fn buffer_layout() -> VertexBufferLayout<'static> {
        pub(crate) const ATTRIBUTES: [VertexAttribute; 3] =
            vertex_attr_array![0 => Float32x3, 1 => Float32x3, 2 => Float32x2];

        VertexBufferLayout {
            array_stride: std::mem::size_of::<Vertex>() as BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &ATTRIBUTES,
        }
    }
}

pub fn vertex(position: Vec3, tint: [f32; 3], tex: Vec2) -> Vertex {
    Vertex {
        position: position.into(),
        tint,
        tex: tex.into(),
    }
}
