use std::{net::SocketAddr, sync::Arc};

use anyhow::Result;
use futures::{sink, SinkExt, StreamExt, TryStreamExt};
use quinn::{ClientConfig, Endpoint};
use tokio_stream::wrappers::UnboundedReceiverStream;
use tokio_util::codec::{FramedRead, FramedWrite, LengthDelimitedCodec};
use tracing::info;

use crate::{
    message::{ClientMessage, ServerMessage},
    utils::{self, AsyncReceiver, SelfSerialize, SyncSender},
};

pub struct RemoteNetwork {
    pub(super) inbound_messages: SyncSender<InboundMessage>,
    pub(super) outbound_messages: AsyncReceiver<OutboundMessage>,
}

#[derive(Debug)]
pub struct InboundMessage {
    pub(super) server_message: ServerMessage,
}

#[derive(Debug)]
pub struct OutboundMessage {
    pub(super) client_message: ClientMessage,
}

impl RemoteNetwork {
    pub fn connect_to_server(self, server_addr: SocketAddr) {
        let inbound_messages = self.inbound_messages.clone();
        let outbound_messages = self.outbound_messages;

        tokio::spawn(utils::report_errors(async move {
            info!("Connecting to server at address {:?}", server_addr);

            let endpoint = Endpoint::client("[::]:0".parse()?)?;
            let connection = endpoint
                .connect_with(configure_client_insecure(), server_addr, "localhost")?
                .await?;
            info!("Established connection");

            let (sender, receiver) = connection.open_bi().await?;
            info!("Opened bidirectional stream");

            tokio::spawn(utils::report_errors(forward_outbound_messages(
                sender,
                outbound_messages,
            )));

            tokio::spawn(utils::report_errors(forward_inbound_messages(
                receiver,
                inbound_messages,
            )));

            Ok(())
        }));
    }
}

async fn forward_inbound_messages(
    receiver: quinn::RecvStream,
    inbound_sender: SyncSender<InboundMessage>,
) -> Result<()> {
    FramedRead::new(receiver, LengthDelimitedCodec::new())
        .map_err(anyhow::Error::from)
        .and_then(|buf| async {
            let server_message = ServerMessage::deserialize(buf)?;
            Ok(InboundMessage { server_message })
        })
        .forward(sink::unfold(
            inbound_sender,
            |inbound_sender, inbound_message| async {
                inbound_sender.send(inbound_message)?;
                Ok(inbound_sender)
            },
        ))
        .await
}

async fn forward_outbound_messages(
    sender: quinn::SendStream,
    outbound_receiver: tokio::sync::mpsc::UnboundedReceiver<OutboundMessage>,
) -> Result<()> {
    let outbound_message_sink = FramedWrite::new(sender, LengthDelimitedCodec::new())
        .sink_map_err(anyhow::Error::from)
        .with(|outbound_message: OutboundMessage| async move {
            outbound_message.client_message.serialize().map(Into::into)
        });

    UnboundedReceiverStream::new(outbound_receiver)
        .map(Result::Ok)
        .forward(outbound_message_sink)
        .await
}

fn configure_client_insecure() -> ClientConfig {
    let crypto = rustls::ClientConfig::builder()
        .with_safe_defaults()
        .with_custom_certificate_verifier(SkipServerVerification::new())
        .with_no_client_auth();

    ClientConfig::new(Arc::new(crypto))
}

struct SkipServerVerification;

impl SkipServerVerification {
    fn new() -> Arc<Self> {
        Arc::new(Self)
    }
}

impl rustls::client::ServerCertVerifier for SkipServerVerification {
    fn verify_server_cert(
        &self,
        _end_entity: &rustls::Certificate,
        _intermediates: &[rustls::Certificate],
        _server_name: &rustls::ServerName,
        _scts: &mut dyn Iterator<Item = &[u8]>,
        _ocsp_response: &[u8],
        _now: std::time::SystemTime,
    ) -> Result<rustls::client::ServerCertVerified, rustls::Error> {
        Ok(rustls::client::ServerCertVerified::assertion())
    }
}
