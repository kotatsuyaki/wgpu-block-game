use std::collections::HashMap;

use anyhow::*;
use glam::{ivec2, vec2, IVec2, Vec2};
use image::{imageops, DynamicImage, GenericImageView, RgbaImage};
use wgpu::{
    AddressMode, Device, Extent3d, FilterMode, ImageCopyTexture, ImageDataLayout, Origin3d, Queue,
    Sampler, SamplerDescriptor, TextureAspect, TextureDescriptor, TextureDimension, TextureFormat,
    TextureUsages, TextureView, TextureViewDescriptor,
};

use crate::block::{Block, BlockBehavior};

pub struct Texture {
    pub texture: wgpu::Texture,
    pub view: TextureView,
    pub sampler: Sampler,
}

type AtlasMap = HashMap<(Block, usize), TextureBB>;

pub struct Atlas {
    texture: Texture,
    map: AtlasMap,
}

#[derive(Debug, Clone)]
pub struct TextureBB {
    min: IVec2,
    max: IVec2,
}

impl Texture {
    pub fn from_bytes(device: &Device, queue: &Queue, bytes: &[u8], label: &str) -> Result<Self> {
        let img = image::load_from_memory(bytes)?;
        Self::from_image(device, queue, &img, Some(label))
    }

    pub fn from_image(
        device: &Device,
        queue: &Queue,
        img: &image::DynamicImage,
        label: Option<&str>,
    ) -> Result<Self> {
        let rgba = img.to_rgba8();
        let dimensions = img.dimensions();

        let size = Extent3d {
            width: dimensions.0,
            height: dimensions.1,
            depth_or_array_layers: 1,
        };
        let texture = device.create_texture(&TextureDescriptor {
            label,
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: TextureFormat::Rgba8UnormSrgb,
            usage: TextureUsages::TEXTURE_BINDING | TextureUsages::COPY_DST,
            view_formats: &[],
        });

        queue.write_texture(
            ImageCopyTexture {
                aspect: TextureAspect::All,
                texture: &texture,
                mip_level: 0,
                origin: Origin3d::ZERO,
            },
            &rgba,
            ImageDataLayout {
                offset: 0,
                bytes_per_row: std::num::NonZeroU32::new(4 * dimensions.0),
                rows_per_image: std::num::NonZeroU32::new(dimensions.1),
            },
            size,
        );

        let view = texture.create_view(&TextureViewDescriptor::default());
        let sampler = device.create_sampler(&SamplerDescriptor {
            address_mode_u: AddressMode::ClampToEdge,
            address_mode_v: AddressMode::ClampToEdge,
            address_mode_w: AddressMode::ClampToEdge,
            mag_filter: FilterMode::Nearest,
            min_filter: FilterMode::Linear,
            mipmap_filter: FilterMode::Linear,
            ..Default::default()
        });

        Ok(Self {
            texture,
            view,
            sampler,
        })
    }
}

impl Atlas {
    const WIDTH: u32 = 64;
    const HEIGHT: u32 = 64;

    pub fn new(device: &Device, queue: &Queue) -> Result<Self> {
        let (image, map) = build_atlas_image()?;
        let image = DynamicImage::from(image);
        let texture = Texture::from_image(device, queue, &image, Some("Block texture atlas"))?;
        Ok(Self { texture, map })
    }

    pub fn box_for(&self, block: &Block, index: usize) -> [Vec2; 4] {
        let TextureBB { min, max } = self.map.get(&(block.clone(), index)).unwrap();
        let w = Self::WIDTH as f32;
        let h = Self::HEIGHT as f32;
        [
            vec2(min.x as f32 / w, max.y as f32 / h),
            vec2(max.x as f32 / w, max.y as f32 / h),
            vec2(max.x as f32 / w, min.y as f32 / h),
            vec2(min.x as f32 / w, min.y as f32 / h),
        ]
    }

    pub fn view(&self) -> &TextureView {
        &self.texture.view
    }

    pub fn sampler(&self) -> &Sampler {
        &self.texture.sampler
    }
}

fn build_atlas_image() -> Result<(RgbaImage, AtlasMap)> {
    let mut atlas_image = image::RgbaImage::new(Atlas::WIDTH, Atlas::HEIGHT);
    let mut x = 0;
    let mut y = 0;
    let mut max_y = 0;

    let mut atlas_map: AtlasMap = HashMap::new();

    for block in Block::iter_variants() {
        for (i, &texture_bytes) in block.textures().iter().enumerate() {
            let texture = image::load_from_memory(texture_bytes)?;
            let (width, height) = texture.dimensions();

            if x + width > Atlas::WIDTH {
                y = max_y;
                x = 0;
            }
            assert!(y + height <= Atlas::HEIGHT);

            imageops::overlay(&mut atlas_image, &texture, x as i64, y as i64);
            atlas_map.insert(
                (block.clone(), i),
                TextureBB {
                    min: ivec2(x as i32, y as i32),
                    max: ivec2((x + width) as i32, (y + height) as i32),
                },
            );

            x += width;
            max_y = y + height;
        }
    }

    Ok((atlas_image, atlas_map))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_build_atlas_image() {
        let (image, _map) = build_atlas_image().unwrap();
        image
            .save_with_format("atlas.png", image::ImageFormat::Png)
            .unwrap();
    }
}
