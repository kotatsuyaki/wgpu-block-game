use std::{collections::HashMap, fmt::Debug, str::FromStr};

use anyhow::{Error, Result};
use itertools::{iproduct, izip};
use serde::{Deserialize, Serialize};

use crate::block::{Block, Grass};

const BLOCK_ARRAY_SIZE: usize = 16 * 256 * 16;

pub struct ChunkStore {
    chunks: HashMap<(i64, i64), Chunk>,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(into = "BakedChunk", try_from = "BakedChunk")]
pub struct Chunk {
    blocks: Box<[Block]>,
    is_subchunk_rendered: Vec<bool>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct BakedChunk {
    palette: HashMap<u8, String>,
    raw: Vec<u8>,
}

impl ChunkStore {
    pub fn new() -> Self {
        Self {
            chunks: HashMap::new(),
        }
    }

    pub fn loaded_coordinates(&self) -> impl Iterator<Item = &(i64, i64)> {
        self.chunks.keys()
    }

    pub fn chunk(&self, (cx, cz): (i64, i64)) -> Option<&Chunk> {
        self.chunks.get(&(cx, cz))
    }

    pub fn chunk_mut(&mut self, (cx, cz): (i64, i64)) -> Option<&mut Chunk> {
        self.chunks.get_mut(&(cx, cz))
    }

    pub fn has_chunk_at(&self, (cx, cz): (i64, i64)) -> bool {
        self.chunks.get(&(cx, cz)).is_some()
    }

    pub fn set_chunk(&mut self, (cx, cz): (i64, i64), chunk: Chunk) {
        self.chunks.insert((cx, cz), chunk);
    }

    pub fn take_chunk(&mut self, (cx, cz): (i64, i64)) -> Option<Chunk> {
        self.chunks.remove_entry(&(cx, cz)).map(|(_k, v)| v)
    }

    pub fn coords(&self) -> impl Iterator<Item = &(i64, i64)> {
        self.chunks.keys()
    }

    pub fn iter(&self) -> impl Iterator<Item = (&(i64, i64), &Chunk)> {
        self.chunks.iter()
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = (&(i64, i64), &mut Chunk)> {
        self.chunks.iter_mut()
    }
}

impl Chunk {
    pub fn new() -> Self {
        let mut chunk = Self {
            blocks: vec![Block::default(); BLOCK_ARRAY_SIZE].into_boxed_slice(),
            is_subchunk_rendered: vec![false; 16],
        };

        for (x, z) in iproduct!(0..16, 0..16) {
            let height = match i64::min(x, z) {
                value @ 0..=7 => value + 1,
                value @ _ => 16 - value,
            };
            for y in 0..height {
                chunk.set_block((x, y, z), Grass.into());
            }
        }

        chunk
    }

    fn random() -> Self {
        let mut chunk = Self {
            blocks: vec![Block::default(); BLOCK_ARRAY_SIZE].into_boxed_slice(),
            is_subchunk_rendered: vec![false; 16],
        };
        for (x, y, z) in iproduct!(0..16, 0..256, 0..16) {
            chunk.set_block((x, y, z), Block::random());
        }
        chunk
    }

    pub fn set_block(&mut self, (x, y, z): (i64, i64, i64), block: Block) {
        self.blocks[Self::index_from_coord((x, y, z))] = block;
    }

    /// Get an instance of the block at `(x, y, z)`.
    ///
    /// The coordinates must be valid local block coordinates.
    pub fn block(&self, (x, y, z): (i64, i64, i64)) -> Block {
        self.blocks[Self::index_from_coord((x, y, z))].clone()
    }

    /// Get an instance of the block at `(x, y, z)`.
    ///
    /// The `y` coordinate can be out of bounds. In this case, `Empty` is returned instead of
    /// panicing like [`Chunk::block`].
    /// The `x` and `z` coordinates must be valid local block coordinates.
    pub fn block_or_empty(&self, (x, y, z): (i64, i64, i64)) -> Block {
        if (0..256).contains(&y) {
            self.block((x, y, z))
        } else {
            crate::block::Empty::default().into()
        }
    }

    pub fn set_all_rendered(&mut self) {
        self.is_subchunk_rendered = vec![true; 16];
    }

    pub fn is_subchunk_rendered(&self, cy: i64) -> bool {
        self.is_subchunk_rendered[cy as usize]
    }

    fn index_from_coord((x, y, z): (i64, i64, i64)) -> usize {
        let (x, y, z) = (x as usize, y as usize, z as usize);
        y * 256 + z * 16 + x
    }
}

impl TryFrom<BakedChunk> for Chunk {
    type Error = Error;

    fn try_from(baked: BakedChunk) -> Result<Self> {
        let mut chunk = Self::new();

        // Map string values to blocks
        let palette: HashMap<u8, Block> = baked
            .palette
            .iter()
            .map(|(&number, name)| {
                Block::from_str(&name)
                    .map(|block| (number, block))
                    .map_err(Error::from)
            })
            .collect::<Result<_>>()?;

        for ((y, z, x), i) in izip!(iproduct!(0..256, 0..16, 0..16), 0..BLOCK_ARRAY_SIZE) {
            chunk.set_block((x, y, z), palette[&baked.raw[i]].clone());
        }

        Ok(chunk)
    }
}

impl From<Chunk> for BakedChunk {
    fn from(chunk: Chunk) -> Self {
        let mut palette = HashMap::<Block, u8>::new();
        let mut next_palette_number = 0;

        let mut raw: Vec<u8> = vec![];

        for (y, z, x) in iproduct!(0..256, 0..16, 0..16) {
            let block = chunk.block((x, y, z));
            let number: u8 = *palette.entry(block).or_insert_with(|| {
                next_palette_number += 1;
                next_palette_number - 1
            });
            raw.push(number);
        }

        // Reverse the map to be used as lookup table when deserializing
        let palette: HashMap<u8, String> = palette
            .into_iter()
            .map(|(block, number)| (number, <&'static str>::from(block).to_string()))
            .collect();

        Self { palette, raw }
    }
}

impl Debug for BakedChunk {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "BakedChunk {{ ... }}")
    }
}

impl Debug for Chunk {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Chunk {{ ... }}")
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_random_serialize_deserialize_chunk() -> Result<()> {
        let original_chunk = Chunk::random();
        let baked_chunk = BakedChunk::from(original_chunk.clone());
        let unbaked_chunk = Chunk::try_from(baked_chunk)?;

        for (x, y, z) in iproduct!(0..16, 0..256, 0..16) {
            assert_eq!(
                original_chunk.block((x, y, z)),
                unbaked_chunk.block((x, y, z)),
                "Block at location {:?} should be the same",
                (x, y, z)
            );
        }
        Ok(())
    }
}
