use serde::{Deserialize, Serialize};

use crate::chunk::Chunk;
#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum ClientMessage {
    Ping,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum ServerMessage {
    Pong,
    UpdateChunk { coord: (i64, i64), chunk: Chunk },
    UnloadChunk { coord: (i64, i64) },
}
