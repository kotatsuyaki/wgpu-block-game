use std::{collections::HashMap, hash::Hash};

use enum_dispatch::enum_dispatch;

use glam::Vec3;
use player::Player;
use uuid::Uuid;

use crate::world::World;

pub struct EntityStore {
    entities: HashMap<Uuid, Entity>,
}

#[enum_dispatch(EntityBehavior)]
#[derive(Debug)]
pub enum Entity {
    Player,
}

pub struct BoundingBox {
    min: Vec3,
    max: Vec3,
}

pub enum EntityQueryData {}
pub enum EntityAction {}

#[enum_dispatch]
pub trait EntityBehavior {
    fn uuid(&self) -> Uuid;
    fn bounding_box(&self) -> BoundingBox;

    fn query(&self, _world: &World) -> Option<EntityQueryData> {
        None
    }
    fn action(&self, _query_data: &EntityQueryData) -> Option<EntityAction> {
        None
    }
    fn update(&mut self, _query_data: &EntityQueryData) {}
}

impl EntityStore {
    pub fn new() -> Self {
        Self {
            entities: HashMap::new(),
        }
    }

    pub fn entity_mut(&mut self, id: &Uuid) -> Option<&mut Entity> {
        self.entities.get_mut(id)
    }

    pub fn entity(&self, id: &Uuid) -> Option<&Entity> {
        self.entities.get(id)
    }

    pub fn add_entity(&mut self, entity: Entity) {
        self.entities.insert(entity.uuid(), entity);
    }

    pub fn iter(&self) -> impl Iterator<Item = (&Uuid, &Entity)> {
        self.entities.iter()
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = (&Uuid, &mut Entity)> {
        self.entities.iter_mut()
    }
}

impl BoundingBox {
    fn new(min: Vec3, max: Vec3) -> Self {
        Self { min, max }
    }
}

impl Hash for Entity {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.uuid().hash(state);
    }
}

impl PartialEq for Entity {
    fn eq(&self, other: &Self) -> bool {
        self.uuid().eq(&other.uuid())
    }
}

impl Eq for Entity {}

mod player {
    use glam::{vec3, Vec3};
    use uuid::Uuid;

    use super::{BoundingBox, EntityBehavior};

    #[derive(Debug)]
    pub struct Player {
        inner: Box<Inner>,
    }

    #[derive(Debug)]
    struct Inner {
        uuid: Uuid,
        position: Vec3,
        velocity: Vec3,
    }

    impl EntityBehavior for Player {
        fn uuid(&self) -> uuid::Uuid {
            self.inner.uuid
        }

        fn bounding_box(&self) -> BoundingBox {
            let inner = &self.inner;
            BoundingBox::new(
                inner.position + vec3(-0.4, 0.0, -0.4),
                inner.position + vec3(0.4, 1.8, 0.4),
            )
        }
    }
}

#[cfg(test)]
mod test {
    use std::mem::size_of;

    use super::*;

    #[test]
    fn test_entity_size() {
        assert_eq!(size_of::<Box<i64>>(), size_of::<Entity>())
    }
}
