use std::hash::Hash;

use enum_dispatch::enum_dispatch;
use rand::prelude::*;
use strum::{EnumDiscriminants, EnumIter, EnumString, FromRepr, IntoEnumIterator, IntoStaticStr};

#[enum_dispatch(BlockBehavior)]
#[derive(
    PartialEq, Eq, Debug, Clone, IntoStaticStr, EnumString, FromRepr, EnumDiscriminants, EnumIter,
)]
#[repr(u8)]
pub enum Block {
    Empty,
    Grass,
}

#[derive(Debug, Clone, Copy)]
pub enum BlockRenderKind {
    None,
    Solid,
}

impl Hash for Block {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let disc = self.discriminant();
        state.write_u8(disc);
    }
}

impl Block {
    fn discriminant(&self) -> u8 {
        // SAFETY: Because `Self` is marked `repr(u8)`, its layout is a `repr(C)` `union`
        // between `repr(C)` structs, each of which has the `u8` discriminant as its first
        // field, so we can read the discriminant without offsetting the pointer.
        unsafe { *<*const _>::from(self).cast::<u8>() }
    }

    pub fn random() -> Self {
        Self::iter().choose(&mut thread_rng()).unwrap()
    }

    pub fn iter_variants() -> impl Iterator<Item = Self> {
        Self::iter()
    }
}

impl Default for Block {
    fn default() -> Self {
        // Self::Empty alone won't compile,
        // because enum_dispatch generates tuple-like variants like Block::Empty(Empty).
        Empty.into()
    }
}

#[derive(Debug, Clone, Copy, EnumIter)]
pub enum Facing {
    PosX,
    NegX,
    PosY,
    NegY,
    PosZ,
    NegZ,
}

impl Facing {
    pub fn iter_variants() -> impl Iterator<Item = Self> {
        Self::iter()
    }

    pub fn displace(&self, coord: (i64, i64, i64)) -> (i64, i64, i64) {
        let (x, y, z) = coord;
        match self {
            Facing::PosX => (x + 1, y, z),
            Facing::NegX => (x - 1, y, z),
            Facing::PosY => (x, y + 1, z),
            Facing::NegY => (x, y - 1, z),
            Facing::PosZ => (x, y, z + 1),
            Facing::NegZ => (x, y, z - 1),
        }
    }
}

#[enum_dispatch]
pub trait BlockBehavior {
    fn render_kind(&self) -> BlockRenderKind {
        BlockRenderKind::Solid
    }

    fn textures(&self) -> &[&'static [u8]] {
        &[]
    }

    fn texture_facing(&self, _facing: Facing) -> usize {
        unimplemented!("texture_facing not implemented")
    }

    fn should_render_facing(&self, _facing: Facing, adjacent_block: &Block) -> bool {
        match adjacent_block.render_kind() {
            BlockRenderKind::Solid => false,
            _ => true,
        }
    }
}

#[derive(PartialEq, Eq, Default, Debug, Clone)]
pub struct Empty;

impl BlockBehavior for Empty {
    fn render_kind(&self) -> BlockRenderKind {
        BlockRenderKind::None
    }
}

#[derive(PartialEq, Eq, Default, Debug, Clone)]
pub struct Grass;

impl BlockBehavior for Grass {
    fn textures(&self) -> &[&'static [u8]] {
        &[include_bytes!("./assets/grass-top-arrow.png")]
    }

    fn texture_facing(&self, _facing: Facing) -> usize {
        0
    }
}

#[cfg(test)]
mod test {
    use std::str::FromStr;

    use super::*;

    #[test]
    fn test_block_string_conversions() {
        let grass = Block::from(Grass);

        assert_eq!("Grass", <&'static str>::from(grass.clone()));
        assert_eq!(grass.clone(), Block::from_str("Grass").unwrap());
    }
}
